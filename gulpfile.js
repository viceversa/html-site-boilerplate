'use strict';

var gulp        = require('gulp');
var browserSync = require('browser-sync').create();
var sass        = require('gulp-sass');
var csso        = require('gulp-csso');
var terser      = require('gulp-terser');
var htmlmin     = require('gulp-htmlmin');
var del         = require('del');

// Gulp task to compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
    return gulp.src('src/scss/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('src/css'))
        .pipe(browserSync.stream());
});

// Gulp task to compile sass and minify css files
gulp.task('styles', function() {
  return gulp.src('src/scss/*.scss')
      .pipe(sass())
      .pipe(csso()) // minify css
      .pipe(gulp.dest('./dist/css'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function() {
  return gulp.src('./src/js/**/*.js')
    // Minify the file
    .pipe(terser())
    // Output
    .pipe(gulp.dest('./dist/js'))
});

// Gulp task to minify HTML files
gulp.task('pages', function() {
  return gulp.src(['./src/**/*.html'])
    .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
    .pipe(gulp.dest('./dist'));
});

// Gulp task to copy image files
gulp.task('images', function() {
  return gulp.src('./src/img/*.**')
    .pipe(gulp.dest('./dist/img'));
});

// Gulp task to lean output directory
gulp.task('clean', () => del(['dist']));

// Gulp task to run Static Server and watch scss/html files
gulp.task('serve', gulp.series('sass', function() {
    browserSync.init({
        server: "./src/"
    });
    gulp.watch("src/scss/*.scss", gulp.series('sass'));
    gulp.watch("src/*.html").on('change', browserSync.reload);
}));

// Gulp task to minify all files
gulp.task('build', gulp.series(
    'clean',
    'styles',
    'scripts',
    'pages',
    'images'
  )
);

// Gulp default task
gulp.task('default', gulp.series('serve'));