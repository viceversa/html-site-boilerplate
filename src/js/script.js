/**
 * Sticky Header 
 */
const header = document.querySelector(".header");
window.onscroll = function() { 
  if (window.pageYOffset >= 10) {
    header.classList.add("header--sticky")
  } else {
    header.classList.remove("header--sticky");
  }
};


/**
 * Toggle Responsive Navigation Menu
 */
const navbar = document.querySelector('.navbar');
navbar.addEventListener('click', function() {
  if (navbar.className === "navbar") {
    navbar.className += " responsive";
  } else {
    navbar.className = "navbar";
  }
});


/**
 * Navigation Menu Items Scroll
 */
// Add listener to brand (logo)
document.querySelector('[href="#home"]').addEventListener('click', doScrolling.bind(null, '#home', 1000));
// Add listeners to all navbar menu items
const menuItems = document.querySelectorAll('.navbar__item');
for (let i = 0; i < menuItems.length; i++) {
  let menuItem = menuItems[i];
  let href = menuItem.getAttribute('href');
  menuItem.addEventListener('click', doScrolling.bind(null, href, 1000));
}

// Animated scrolling
function doScrolling(element, duration) {
	var startingY = window.pageYOffset;
  var elementY = window.pageYOffset + document.querySelector(element).getBoundingClientRect().top;
  var targetY = document.body.scrollHeight - elementY < window.innerHeight ? document.body.scrollHeight - window.innerHeight : elementY;
	var diff = targetY - startingY;
  var easing = function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 };
  var start;
  if (!diff) return;
	window.requestAnimationFrame(function step(timestamp) {
    if (!start) start = timestamp;
    var time = timestamp - start;
    var percent = Math.min(time / duration, 1);
    percent = easing(percent);
    window.scrollTo(0, startingY + diff * percent);
    if (time < duration) {
      window.requestAnimationFrame(step);
    }
  })
}

/**
 * Modal Cards
 */
const cards = document.getElementsByClassName('modal-card');
for (let i = 0; i < cards.length; i++) {
  let card = cards[i];
  card.addEventListener('click', function() {
    let modal = document.getElementById(this.dataset.modal);
    modal.style.display = "block";
    document.body.style.overflow = "hidden";
  }, false);
}
const closeBtns = document.getElementsByClassName('modal__close');
for (let j = 0; j < closeBtns.length; j++) {
  let closeBtn = closeBtns[j];
  closeBtn.addEventListener('click', function() {
    let modal = document.getElementById(this.dataset.close);
    modal.style.display = "none";
    document.body.style.overflow = "auto";
  }, false)
}
const modals = document.getElementsByClassName('modal');
for (let k = 0; k < modals.length; k++) {
  let modal = modals[k];
  window.addEventListener('click', function(e) {
    if (e.target == modal) {
      modal.style.display = "none";
      document.body.style.overflow = "auto";
    }
  }, false);
}

// remove videos and slides
// let timeCardIcons = document.querySelectorAll('.timetable-card__icons');
// timeCardIcons = Array.from(timeCardIcons);
// for (timeCardIcon of timeCardIcons) {
//   timeCardIcon.parentNode.removeChild(timeCardIcon);
// }
// let modalVideos = document.querySelectorAll('.modal-video');
// modalVideos = Array.from(modalVideos);
// for (modalVideo of modalVideos) {
//   modalVideo.parentNode.removeChild(modalVideo);
// }

// /**
//  * Process contact form
//  */
// const submit = document.getElementById('contact-form-submit');
// const name_input = document.getElementById('contact-form-name');
// const email_input = document.getElementById('contact-form-email');
// const message_input = document.getElementById('contact-form-message');
// submit.addEventListener('click', function(e) {
//   e.preventDefault();

//   // Clear all error fields
//   const error_messages = document.querySelectorAll('.tooltip');
//   for (let i = 0; i < error_messages.length; i++) {
//     error_messages[i].parentNode.removeChild(error_messages[i]);
//   }

//   // Validate inputs
//   let name = name_input.value;
//   if (name == '' || name == 'type your name') {
//     const name_input_tooltip = document.createElement('span');
//     name_input_tooltip.classList.add('tooltip');
//     name_input_tooltip.textContent = name_input.dataset.originalTitle;
//     name_input.parentNode.insertBefore(name_input_tooltip, name_input);
//     name_input.focus();
//     return false;
//   }
//   let email = email_input.value;
//   if (email == '' || email == 'type your email') {
//     const email_input_tooltip = document.createElement('span');
//     email_input_tooltip.classList.add('tooltip');
//     email_input_tooltip.textContent = email_input.dataset.originalTitle;
//     email_input.parentNode.insertBefore(email_input_tooltip, email_input);
//     email_input.focus();
//     return false;
//   }
//   const filter = /^[a-zA-Z0-9]+[a-zA-Z0-9_.-]+[a-zA-Z0-9_-]+@[a-zA-Z0-9.-]+.[a-zA-z0-9]{2,4}$/;
//   if (!filter.test(email)) {
//     const email_input_tooltip = document.createElement('span');
//     email_input_tooltip.classList.add('tooltip');
//     email_input_tooltip.textContent = 'invalid email address';
//     email_input.parentNode.insertBefore(email_input_tooltip, email_input);
//     email_input.focus();
//     return false;
//   }
//   let message = message_input.value;
//   if (message == '' || message == 'type your message') {
//     const message_input_tooltip = document.createElement('span');
//     message_input_tooltip.classList.add('tooltip');
//     message_input_tooltip.textContent = message_input.dataset.originalTitle;
//     message_input.parentNode.insertBefore(message_input_tooltip, message_input);
//     message_input.focus();
//     return false;
//   }

//   // Create Modal with Spinner
//   const modalAjax = document.querySelector('.modal-ajax');
//   const modalAjaxSpinner = document.querySelector('.modal-ajax__spinner');
//   const modalAjaxMessage = document.querySelector('.modal-ajax__message');
//   modalAjax.style.display = 'block';
//   modalAjaxSpinner.style.display = 'block';
//   document.body.style.overflow = "hidden";

//   // Send data to server for processing
//   const xhr = new XMLHttpRequest();
//   const url = "contact-form.php";
//   const params = 'name=' + name + '&email=' + email + '&message=' + message;
//   xhr.open("POST", url, true);
//   xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//   xhr.onreadystatechange = function() {
//     if (xhr.readyState == 4 && xhr.status == 200) {
//       modalAjaxSpinner.style.display = 'none';
//       modalAjaxMessage.style.display = 'block';
//       modalAjaxMessage.textContent = 'Message Sent';
      
//       // Remove message after 3 sec
//       setTimeout(function() {
//         modalAjaxMessage.textContent = '';
//         modalAjaxMessage.style.display = 'none';
//         modalAjax.style.display = 'none';
//         document.body.style.overflow = "initial";
//       }, 3000);

//       // Clear fields
//       name_input.value = '';
//       email_input.value = '';
//       message_input.value = '';
//     }
//   }
//   xhr.send(params);
//   return false;
// });